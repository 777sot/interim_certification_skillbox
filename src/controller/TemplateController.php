<?php


/**
 * users/login/
 * $users->login
 * connecting the template
 *
 */

if ($_SERVER['REQUEST_METHOD'] === 'GET' && $_REQUEST['rout'] === 'users/login/' && $_REQUEST['password']) {

    if (password_verify($_REQUEST['password'], $result[0]['password']) === true) {

        $_SESSION['role'] = $result[0]['role'];
        $_SESSION['first_name'] = $result[0]['first_name'];
        $_SESSION['email'] = $result[0]['email'];

        if ($result[0]['role'] === 'ADMIN_ROLE') {

            echo $twigAdmin->render('admin_users.html.twig', [
                'message' => 'Добро пожаловать ' . $result[0]['first_name'],
                'url' => 'http://' . $_SERVER['HTTP_HOST'] . '/admin/user/',
                'user' => 0,
            ]);

        } elseif ($result[0]['role'] === 'USER_ROLE'){

            echo $twig->render('user_login_.html.twig', [
                'message' => 'Добро пожаловать ' . $result[0]['first_name'],
                'user' => $result[0],
            ]);
        }

    } else {
        echo $twig->render('user_login_.html.twig', [
            'message' => 'Такого пользователя ' . $_SESSION['first_name'] . ' не существует',
        ]);
    }
}

/**
 * users/
 * $users->listUsers
 * connecting the template
 */

if ($_SERVER['REQUEST_METHOD'] === 'GET' && $_REQUEST['rout'] === 'user/' && !isset($_REQUEST['REQUEST_METHOD'])) {
    echo $twig->render('users.html.twig', [
        'result' => $result[0]
    ]);
}

/**
 * user/id
 * $users->getUserId
 * Json
 * connecting the template
 */

if ($id && $_SERVER['REQUEST_METHOD'] === 'GET' && $_REQUEST['rout'] === 'user/' . $id) {

    if (!empty($urlList['user/' . $id]['GET']) && $result[0] !== 'false') {

        $userJson = json_encode($result[0], JSON_THROW_ON_ERROR);

        echo $twig->render('users_json_id.html.twig', [
            'user' => $result[0],
            'userJson' => $userJson,

        ]);
    } else {
        echo $twig->render('users_json_id.html.twig', [
            'massage' => 'Такого пользователя не найдено',

        ]);
    }

}

/**
 * users/logout
 * $users->logout
 * session_unset
 * connecting the template
 */

if ($_SERVER['REQUEST_METHOD'] === 'GET' && $_REQUEST['rout'] === 'users/logout/') {

    echo $twig->render('user_logout_.html.twig', [
        'message' => 'Сессия удалена, Вы вышли из профиля',
    ]);

}

/**
 * users/
 * $users->addUser
 * Add user in MySQL
 */
if ($_SERVER['REQUEST_METHOD'] === 'POST' && $_REQUEST['rout'] === 'users/') {

    if ($urlList['users/']['POST'] === true) {
        $arrayData = [
            'email' => $_REQUEST['email'],
            'role' => $_REQUEST['role'],
            'date_created' => date('Y-m-d')
        ];

        echo $twig->render('user_add.html.twig', [
            'message' => 'Ползователь ' . $_REQUEST['first_name'] . ' добавлен в базу данных',
            'user' => $arrayData,
        ]);
    } else {
        echo $twig->render('user_add.html.twig', [
            'message' => 'Ползователь ' . $_REQUEST['first_name'] . '  НЕ добавлен в базу данных',
        ]);
    }

}

/**
 * users/id
 * $users->updateUser
 * Update user in MySQL
 */
if (($_SERVER['REQUEST_METHOD'] === 'PUT' && $_REQUEST['rout'] === 'users/' && $_REQUEST['id']) ||
    ($urlList['users/']['PUT'] === true && $_REQUEST['REQUEST_METHOD'] === 'PUT')) {

    if ($urlList['users/']['PUT'] === true) {
        $arrayData = [
            'email' => $_REQUEST['email'],
            'role' => $_REQUEST['role'],
            'date_created' => date('Y-m-d')
        ];

        echo $twig->render('user_update.html.twig', [
            'message' => 'У ползователя ' . $_REQUEST['first_name'] . ' обновлены данные',
            'user' => $arrayData,
        ]);
    } else {
        echo $twig->render('user_update.html.twig', [
            'message' => 'У ползователя ' . $_REQUEST['first_name'] . '  НЕ обновились данные',
        ]);
    }
}

/**
 * user/id
 * $users->deleteUser
 * User delete
 */
if ($id && $_SERVER['REQUEST_METHOD'] === 'DELETE' && $_REQUEST['rout'] === 'user/' . $id) {
    if ($urlList['user/' . $id]['DELETE'] === true) {

        echo $twig->render('user_delete.html.twig', [
            'message' => 'Пользователь успешно удален',
        ]);
    } else {
        echo $twig->render('user_delete.html.twig', [
            'message' => 'Пользоваеля не удалось удалить',
        ]);
    }
}

/**
 * users/reset_password/
 * $users->resetPassword
 * connecting the template
 */

if ($_SERVER['REQUEST_METHOD'] === 'GET' && $_REQUEST['rout'] === 'users/reset_password/') {

    if ($urlList['users/reset_password/']['GET'] === true) {
        echo $twig->render('users_reset_password.html.twig', [
            'message' => 'На почтовый ящик вам направленно письмо, в котором ссылка для смены пароля',
        ]);
    } else {
        echo $twig->render('users_reset_password.html.twig', [
            'message' => 'Не верно указан e-mail или такого пользователя не существует',
            'messageMail' => 'Сообщение не доставленно ' . $urlList['users/reset_password/']['GET'],
        ]);
    }
}

/**
 * admin/user/
 * $admin->listUsers
 * connecting the template
 */


if ($_SERVER['REQUEST_METHOD'] === 'GET'
    && $_SESSION['role'] === 'ADMIN_ROLE'
    && $_REQUEST['rout'] === 'admin/user/'
) {

    echo $twigAdmin->render('admin_users.html.twig', [
        'message' => 'Добро пожаловать ' . $result[0]['first_name'],
        'user' => $result[0],
        'host' => 'http://' . $_SERVER['HTTP_HOST'],
    ]);
}

/**
 * admin/user/id
 * $admin->getUserId($id)
 * connecting the template
 */

if (($_REQUEST['rout'] === 'admin/user/' . $id)
    && $_SERVER['REQUEST_METHOD'] === 'GET'
    && $_SESSION['role'] === 'ADMIN_ROLE'
    && $_REQUEST['method'] !== 'DELETE'
    && $_REQUEST['method'] !== 'PUT'
) {

    echo $twigAdmin->render('admin_get_users_id.html.twig', [
        'message' => 'Добро пожаловать ' . $_SESSION['first_name'],
        'user' => $result[0],
        'url' => 'http://' . $_SERVER['HTTP_HOST'] . '/admin/user/'
    ]);
}


/**
 * user/search/email
 * $users->getUserByEmail
 * connecting the template
 */

if (($_REQUEST['rout'] === 'user/search/' . $email)
    && $_SERVER['REQUEST_METHOD'] === 'GET'
){
    echo $twigAdmin->render('admin_get_users_id.html.twig', [
        'message' => 'Поиск пользователя по email',
        'user' => $result[0][0],
        'url' => 'http://' . $_SERVER['HTTP_HOST'] . '/admin/user/'
    ]);
}

/**
 * admin/user/id
 * $admin->updateUser()
 * connecting the template
 */
if ($_REQUEST['rout'] === 'admin/user/' . $id
    && $_SESSION['role'] === 'ADMIN_ROLE'
    && $_REQUEST['method'] === 'PUT'
) {

    echo $twigAdmin->render('admin_user_update.html.twig', [
        'message' => 'Внесите изменения в данных пользователя ',
        'user' => $request->getHeaders() ?: $admin->getUserId($id),
        'url' => 'http://' . $_SERVER['HTTP_HOST'] . '/admin/user/',
    ]);
}

/**
 * file/
 * $file->fileList()
 * connecting the template
 */

if (($_REQUEST['rout'] === 'file/'
        && in_array(mb_strtolower($_SESSION['first_name'] . '_' . $_SESSION['email']),
            scandir('sharing/'), true) === true && !$_REQUEST['newname']) || ($_REQUEST['rout'] === 'file/'
        && in_array(mb_strtolower($_SESSION['first_name'] . '_' . $_SESSION['email']),
            scandir('sharing/'), true) === true && !$_REQUEST['newname'])
) {

//    $function = new TwigFunction('showTree', File::showTree($result[0]));
//    $twigFile->addFunction($function);

    echo $twigFile->render('file_list.html.twig', [
        'message' => 'Ваши файлы ',
        'file' => $result[0],
        'url' => HTTP_HOST . '/' . PATH_DIR,
        'dir' => $file->getAllDir(),
    ]);
}

/**
 * file/id
 * $file->fileGetId()
 * connecting the template
 */
if (($_REQUEST['rout'] === 'file/' . $id
        && in_array(mb_strtolower($_SESSION['first_name'] . '_' . $_SESSION['email']),
            scandir('sharing/'), true) === true) && $_SERVER['REQUEST_METHOD'] === 'GET'
) {

    echo $twigFile->render('file_get_info.html.twig', [
        'message' => 'Информация о файле',
        'file' => $result[0],
    ]);
}

/**
 * file/
 * $file->fileReName()
 * connecting the template
 */
if (($_REQUEST['rout'] === 'file/'
        && in_array(mb_strtolower($_SESSION['first_name'] . '_' . $_SESSION['email']),
            scandir('sharing/'), true) === true) && $_REQUEST['newname']
) {

    echo $twigFile->render('file_rename.html.twig', [
        'message' => 'Фаил переименован или перенесён в другую директорию',
        'file' => $file->fileList(),
        'dir' => $file->getAllDir(),
    ]);
}

/**
 * directory/id
 * $file->directoryGetId
 * connecting the template
 */
if (($_REQUEST['rout'] === 'directory/' . $id
    && in_array(mb_strtolower($_SESSION['first_name'] . '_' . $_SESSION['email']),
        scandir('sharing/'), true) === true)
) {

    echo $twigFile->render('file_get_dir.html.twig', [
        'message' => 'Информация о директории',
        'file' => $result[0],
    ]);
}


