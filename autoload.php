<?php

use Twig\Environment;
use Twig\Loader\FilesystemLoader;


/**
 * @param $className
 * @return bool
 */
function loadControllerClass($className)
{
    $fileName = __DIR__ . '\src\controller\\' . $className . '.php';

    if (file_exists($fileName)) {

        require_once $fileName;
    }

    return false;
}

;

if (spl_autoload_register('loadControllerClass') === false) {

    echo 'Ошибка подключения класса контроллера' . PHP_EOL;
}


/**
 * templates twig
 */

$loader = new FilesystemLoader(__DIR__ . '\templates');
$twig = new Environment($loader);

$loaderAdmin = new FilesystemLoader(__DIR__ . '\templates\admin\\');
$twigAdmin = new Environment($loaderAdmin);

$loaderFile = new FilesystemLoader(__DIR__ . '\templates\file\\');
$twigFile = new Environment($loaderFile);


/**
 * User::class
 * MySQL connect
 * @var User $users
 */
$users = new User('localhost', 'sharing', 'root', '');

/**
 * Admin::class
 * @var Admin $admin
 */
$admin = new Admin('localhost', 'sharing', 'root', '');

/**
 * controller\File::class
 * @var File $file
 */
$file = new File('localhost', 'sharing', 'root', '');

/**
 * ControllerRout::class
 */
$controller = new ControllerRout();
