<?php

session_start();

define('PATH_DIR', 'sharing/' . mb_strtolower($_SESSION['first_name'] . '_' . $_SESSION['email']));
define('HTTP_HOST', 'http://' . $_SERVER['HTTP_HOST']);

use Origin\Filesystem\File;
use GuzzleHttp\Psr7\Request;

require_once __DIR__ . '\vendor\autoload.php';
require_once __DIR__ . '\autoload.php';

/**
 * Get email from URL
 */
if (strpos($_REQUEST['rout'], 'user/search/') !== false) {

    $REQUEST_EMAIL = $controller->parsUrlGetEmail();

    if ($REQUEST_EMAIL) {
        $email = $REQUEST_EMAIL;
    } else {
        $email = 'null';
    }
}

/**
 * Get id from URL
 */
if (!is_array($REQUEST_URI = $controller->parsUrlGetId())) {
    if ($REQUEST_URI > 0) {
        $id = $REQUEST_URI;
    } else {
        $id = 'null';
        $file_id = null;
    }
} elseif (is_array($REQUEST_URI)) {

    $file_id = $REQUEST_URI[0];
    $id = $REQUEST_URI[1];

}

/**
 * Array rout
 * @var User $users
 * @var File $file
 * @var Admin $admin
 */

$urlList = [
    'user/' => [
        'GET' => $users->listUsers(),
        'POST' => $users->addUser(),
        'PUT' => $users->updateUser(),
    ],
    'user/' . $id => [
        'GET' => $users->getUserId($id),
        'DELETE' => $users->deleteUser($id),
    ],
    'users/login/' => [
        'GET' => $users->login($_REQUEST['email'], $_REQUEST['password'])
    ],
    'users/logout/' => [
        'GET' => $users->logout()
    ],
    'users/reset_password/' => [
        'GET' => $users->resetPassword($_GET['email'], $_GET['new_password'])
    ],
    'user/search/' . $email => [
        'GET' => $users->getUserByEmail($email)
    ],
    'admin/user/' => [
        'GET' => $admin->listUsers(),
        'PUT' => $admin->updateUser(),
    ],
    'admin/user/' . $id => [
        'GET' => $admin->getUserId($id),
        'DELETE' => $admin->deleteUser($id),
    ],
    'file/' => [
        'GET' => $file->fileList(),
        'POST' => $file->fileAdd($_POST['destinationDir'], $_FILES['file']),
        'PUT' => $file->fileReName($_REQUEST['file'], $_REQUEST['dir'], $_REQUEST['newname']),
    ],
    'file/' . $id => [
        'GET' => $file->fileGetId($id),
        'DELETE' => $file->fileDelete($id),
    ],
    'files/share/' . $id => [
        'GET' => $file->getListUserAccessFromFile($id),
    ],
    'files/share/' . $file_id . '/' . $id => [
        'PUT' => $file->addFileAccessUser($file_id, $id),
        'DELETE' => $file->deleteAccessUserForFile($file_id, $id),
    ],
    'directory/' => [
        'POST' => $file->directoryAdd($_REQUEST['pathdir'], $_REQUEST['namedir']),
        'PUT' => $file->directoryReName($_REQUEST['olddir'], $_REQUEST['newdir']),
    ],
    'directory/' . $id => [
        'GET' => $file->directoryGetId($id),
        'DELETE' => $file->directoryDelete($id),
    ],
];

/**
 * Get Object MySQL
 */

$result = $controller->getObjectMySQL($urlList);

/**
 * GuzzleHttp
 */

if (($id && $_REQUEST['rout'] === 'admin/user/' . $id) || ($id && $_REQUEST['rout'] === 'users/' . $id)) {

    $request = new Request($_SERVER['REQUEST_METHOD'] ? 'PUT' : 'DELETE', $_REQUEST['rout'], [
        'id' => $result[0]['id'],
        'first_name' => $result[0]['first_name'],
        'email' => $result[0]['email'],
        'password' => $result[0]['password'],
        'role' => $result[0]['role'],
        'date_created' => $result[0]['date_created']
    ]);
}

/**
 * connect templates
 */
require_once __DIR__ . '\src\controller\TemplateController.php';


dump($_SESSION, $result);