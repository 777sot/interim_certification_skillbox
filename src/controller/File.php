<?php


/**
 * Class File
 */
class File extends User
{
    /**
     * User constructor.
     * @param $host
     * @param $dbname
     * @param $user
     * @param $pass
     * @param int $port
     */
    public function __construct($host, $dbname, $user, $pass, $port = 3306)
    {
        parent::__construct($host, $dbname, $user, $pass, $port);
    }

    /**
     * @var DirectoryIterator|null
     */
    private $directoryIterator;


    /**
     * @param $dir
     * @return SplFileInfo
     */
    public function splFileInfo($dir)
    {
        if (!$dir) {
            return null;
        }
        try {
            return new SplFileInfo($dir);
        } catch (Exception $e) {

            return 'ошибка создания экземпляра класса SplFileInfo' . $e->getMessage();
        }

    }

    /**
     * @param $dir
     * @return RecursiveIteratorIterator
     */
    public function recursiveDirectoryIterator($dir)
    {
        if (!$dir) {
            return null;
        }
        try {
            return new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dir));
        } catch (Exception $e) {

            return 'ошибка создания экземпляра класса RecursiveDirectoryIterator' . $e->getMessage();
        }

    }

    /**
     * @param string $directory Путь к директории.
     * @return array Список файлов в директории.
     */
    public function fileList($directory = null)
    {

        if ($_SESSION['email'] !== null) {
            if (strpos($directory, 'sharing/') === false) {
                if ($directory !== null) {
                    $directory = PATH_DIR . '/' . $directory;
                } else {
                    $directory = PATH_DIR;
                }
            }

            try {
                $iterator = $this->recursiveDirectoryIterator($directory);

                foreach ($iterator as $file) {
                    $result[] = rtrim($iterator->getSubPathName(), '.');
                }

                return array_filter(array_unique($result));
            } catch (Exception $e) {

                return 'Ошибка формирования списка' . $e->getMessage();
            }

        }

    }

    /**
     * @param $id
     * @return null
     */
    public function fileGetId($id)
    {
        if ($id && $_REQUEST['rout'] === 'file/' . $id
            && $_SESSION['email'] !== null
            && $_SERVER['REQUEST_METHOD'] === 'GET'
        ) {

            try {
                $fileList = $this->fileList();
                foreach ($fileList as $k => $v) {
                    if (str_contains($v, $id) !== false) {
                        $file = PATH_DIR . '/' . $v;
                        break;
                    }
                }

                if (!$iterator = $this->splFileInfo($file)) {
                    return 'Ошибка поиска !!!';
                }

                $fileInfo['Path'] = $iterator->getPath();
                $fileInfo['Owner'] = $iterator->getOwner();
                $fileInfo['Perms'] = $iterator->getPerms();
                $fileInfo['Group'] = $iterator->getGroup();
                $fileInfo['Pathname'] = $iterator->getPathname();
                $fileInfo['Filename'] = $iterator->getFilename();
                $fileInfo['RealPath'] = $iterator->getRealPath();
                $fileInfo['Basename'] = $iterator->getBasename();
                $fileInfo['Extension'] = $iterator->getExtension();
                $fileInfo['Type'] = $iterator->getType();
                $fileInfo['Size'] = $iterator->getSize();
                $fileInfo['Inode'] = $iterator->getInode();
                $fileInfo['ATime'] = $iterator->getATime();
                $fileInfo['CTime'] = $iterator->getCTime();
                $fileInfo['MTime'] = $iterator->getMTime();
                $fileInfo['Perms'] = $iterator->getPerms();
                $fileInfo['LinkTarget'] = $iterator->getLinkTarget();
                $fileInfo['Dir'] = $iterator->isDir();
                $fileInfo['File'] = $iterator->isFile();
                $fileInfo['Link'] = $iterator->isLink();
                $fileInfo['Writable'] = $iterator->isWritable();
                $fileInfo['Readable'] = $iterator->isReadable();

                return $fileInfo;
            } catch (Exception $e) {
                return 'Ошибка получения данных' . $e->getMessage();
            }

        } else {
            return null;
        }
    }

    /**
     * @param $dir
     * @param string $inputName Имя поля для загрузки файла.
     * @return Exception|null
     */
    public function fileAdd($dir, $inputName)
    {
        if ($_SESSION['email'] !== null) {
            if (!$dir || !$inputName) {
                return null;
            }
            $destinationDir = '';

            if ($_SERVER['REQUEST_METHOD'] === 'POST' && $_REQUEST['rout'] === 'file/' && $dir !== $destinationDir) {

                $destinationDir = PATH_DIR . '\\' . $dir;
            }
            if (!isset($inputName)) {
                return null;
            }
            $file = $inputName;
            if ($file['error'] !== 0) {
                return null;
            }
            $extension = pathinfo($inputName['name'], PATHINFO_EXTENSION);
            $fileName = pathinfo($inputName['name'],  PATHINFO_FILENAME);

            $filename =  $fileName .'_'. time() . '.' . $extension;
            if (!move_uploaded_file($inputName['tmp_name'], $destinationDir . '\\' . $filename)) {
                return new Exception('Ошибка загрузки файла');
            }

            header('Location:  ' . HTTP_HOST . '/file/');
            //return $destinationDir . '\\' . $filename;
        }

    }


    /**
     * @param $newFileName
     * @param $oldFilePath
     * @param $newFilePath
     * @return bool
     */
    public function fileReName($oldFilePath, $newFilePath, $newFileName)
    {
        if ($_SESSION['email'] !== null && $_REQUEST['rout'] === 'file/' && $_REQUEST['newname']) {

            if (!$oldFilePath || !$newFilePath || !$newFileName) {
                return null;
            }

            $extension = pathinfo(PATH_DIR . '/' . $oldFilePath, PATHINFO_EXTENSION);

            if (!file_exists(PATH_DIR . '/' . $oldFilePath)) {
                return false;
            }

            $newFilePath = PATH_DIR . '/' . $newFilePath . '/' . $newFileName . '_' . time() . '.' . $extension;
            try {
                return rename(PATH_DIR . '/' . $oldFilePath, $newFilePath);
            } catch (Exception $e) {
                $e->getMessage();
            }

        }

    }

    /**
     * @param $id
     */
    public function fileDelete($id)
    {
        if ($id && $_REQUEST['rout'] === 'file/' . $id
            && $_SESSION['email'] !== null
            && $_SERVER['REQUEST_METHOD'] === 'DELETE'
        ) {
            try {

                $fileList = $this->fileList();
                foreach ($fileList as $k => $v) {
                    if (str_contains($v, $id) !== false) {
                        $file = PATH_DIR . '/' . $v;
                        break;
                    }
                }
                unlink($file);
                header('Location:  ' . HTTP_HOST . '/file/');
            } catch (Exception $e) {
                $e->getMessage();
            }

        }

    }

    /**
     * @param $pathDir
     * @param $nameDir
     * @return bool|null
     */
    public function directoryAdd($pathDir = null, $nameDir)
    {
        if ($_REQUEST['namedir'] && $_REQUEST['rout'] === 'directory/' && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $dirNew = PATH_DIR . '/' . $_REQUEST['pathdir'] . $_REQUEST['namedir'] . '_' . time();

            try {
                return mkdir($dirNew);
            } catch (Exception $e) {
                return 'Не удалось создать директорию' . $e->getMessage();
            }
        }

    }

    /**
     * @param $oldFolderName
     * @param $newFolderName
     * @return string
     */
    public function directoryReName($oldFolderName, $newFolderName)
    {
        if ($_REQUEST['rout'] === 'directory/'
            && $_SESSION['email'] !== null
            && $_SERVER['REQUEST_METHOD'] === 'PUT'
        ) {
            try {

                return rename(PATH_DIR . '\\' . $oldFolderName, PATH_DIR . '\\' . $newFolderName . '_' . time());

            } catch (Exception $e) {

                return 'Не удалось переименовать директорию ' . $e->getMessage();
            }
        }

    }

    /**
     * @param $id
     * @return array|false|string
     */
    public function directoryGetId($id)
    {
        if ($_REQUEST['rout'] === 'directory/' . $id && $_SERVER['REQUEST_METHOD'] === 'GET') {
            try {

                if (!$allDir = $this->getAllDir()) {
                    return 'Ошибка получения данных getAllDir()';
                }
                foreach ($allDir as $k => $v) {
                    if (str_contains($v, $id) !== false) {
                        $file = $v;
                        break;
                    }
                }
                return $this->getUrlFileFromDir($file);

            } catch (Exception $e) {
                return 'Ошибка получения данных ' . $e->getMessage();
            }

        } else {
            return null;
        }
    }

    /**
     * @param $id
     * @return bool
     */
    public function directoryDelete($id)
    {
        if ($_SERVER['REQUEST_METHOD'] === 'DELETE' && $_SESSION['email']) {
            if ($_REQUEST['rout'] !== 'directory/' . $id) {
                return false;
            }

            try {
                $allDir = $this->getAllDir();
                foreach ($allDir as $k => $v) {
                    if (str_contains($v, $id) !== false) {
                        $dir = $v;
                        break;
                    }
                }
            } catch (Exception $e) {
                return 'Ошибка получения данных' . $e->getMessage();
            }

            if ($file = $this->getUrlFileFromDir($dir)) {

                foreach ($file as $value) {
                    if (file_exists($value)) {
                        unlink($value);
                    }

                }
            }

            try {
                return rmdir(PATH_DIR . '/' . $dir);
            } catch (Exception $e) {
                return 'Не удалось удалить директорию' . $e->getMessage();
            }

        } else {

            return null;
        }

    }

    /**
     * @param null $directory
     * @return array
     */
    public function getAllDir($directory = null)
    {
        if ($_SESSION['email'] !== null) {
            if (strpos($directory, 'sharing/') === false) {
                $directory = PATH_DIR;
            }
        }
        try {
            $iterator = $this->recursiveDirectoryIterator($directory);
        } catch (Exception $e) {
            $e->getMessage();
            $e->getFile();
        }


        foreach ($iterator as $file) {
            $result[] = $iterator->getSubPath();
        }

        return array_filter(array_unique($result));
    }

    /**
     * @param $catalog
     * @return array
     */
    public function getUrlFileFromDir($catalog = null)
    {
        $dir = PATH_DIR . '/' . $catalog;
        $iterator = new DirectoryIterator($dir);
        if ($iterator->isDir()) {
            foreach ($iterator as $fileinfo) {
                if (!$fileinfo->isDot()) {
                    $file[] = $fileinfo->getPathname();
                    //$file[] = $fileinfo->getFilename();
                }
            }
            return $file;
        } else {
            return null;
        }
    }

    /**
     * @param $file_id
     * @return mixed
     */
    public function getListUserAccessFromFile($file_id)
    {
        if ($_REQUEST['rout'] === 'files/share/' . $file_id && $_SERVER['REQUEST_METHOD'] === 'GET') {
            return $this->connection
                ->query("SELECT f.file_id, u.id, u.first_name, u.email, u.role  
                                    FROM user_to_file f JOIN users u 
                                    WHERE f.file_id =  $file_id 
                                      AND f.user_id = u.id"
                )->fetchAll();
        }
    }

    /**
     * @param $file_id
     * @param $user_id
     * @return bool
     */
    public function deleteAccessUserForFile($file_id, $user_id)
    {
        if ($file_id && $user_id
            && $_SERVER['REQUEST_METHOD'] === 'DELETE'
            && $_REQUEST['rout'] === 'files/share/' . $file_id .'/' . $user_id
        )
       try{
           return $this->connection->prepare('DELETE FROM user_to_file WHERE file_id = :file_id AND user_id = :user_id')
               ->execute([
                   'file_id' => $file_id,
                   'user_id' => $user_id
               ]);

       }catch (Exception $e) {
           return $e->getMessage();
       }

    }

    /**
     * @param $file_id
     * @param $user_id
     * @return string
     */
    public function addFileAccessUser($file_id, $user_id)
    {
        if (strpos($_REQUEST['rout'], 'files/share/') !== false && $_SERVER['REQUEST_METHOD'] === 'PUT') {

            if (!$user = $this->getUserIdForClassFile($user_id)) {
                throw new RuntimeException('Не верный id пользователя');
            }

            $fileList = $this->fileList();
            foreach ($fileList as $k => $v) {
                if (str_contains($v, $file_id) !== false) {
                    $file = PATH_DIR . '/' . $v;
                    break;
                }
            }

            if (is_file($file)) {
                if (!$iterator = $this->splFileInfo($file)) {
                    throw new RuntimeException('Не верный id файла');
                }
            } else {
                throw new RuntimeException('Такого файла не существует');
            }
            if ($user && $file) {
                $statement = $this->connection->prepare(
                    "INSERT INTO `user_to_file` (`user_id`,`file_id`)  VALUES ($user_id, $file_id)"
                );
                if ($statement->execute()) {
                    return 'Данные успешно добавлены';
                }
                throw new RuntimeException('Ошибка добавления данных');
            }
        }
    }
}
