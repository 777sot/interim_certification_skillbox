-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 15 2023 г., 11:06
-- Версия сервера: 5.7.33-log
-- Версия PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `sharing`
--
CREATE DATABASE IF NOT EXISTS `sharing` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `sharing`;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
                         `id` int(255) NOT NULL,
                         `first_name` varchar(255) NOT NULL,
                         `email` varchar(255) NOT NULL,
                         `password` varchar(255) NOT NULL,
                         `role` varchar(100) NOT NULL,
                         `date_created` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `first_name`, `email`, `password`, `role`, `date_created`) VALUES
(18, 'Admin', 'admin@mail.com', '$2y$10$miVYofmOzIV68scFmygD7O4SddPQjfWgYBa77tMPMFBniMP3bWk9u', 'ADMIN_ROLE', '2023-04-05'),
(19, 'Sergey Ivanov', 'sergey@mail.com', '$2y$10$20l4wzvEl2dPjcOhdO63wORoAePbGP4Ll04Br/qYy7yNsz682R2Ym', 'USER_ROLE', '2023-04-09'),
(24, 'Prodobrey', 'prodobrey@mail.com', '$2y$10$0tVGzz3lwAH3UERrOqktqeFeAK/BzHUrqtkdtagQ6HpB4XSFqx4V.', 'USER_ROLE', '2023-04-09');

-- --------------------------------------------------------

--
-- Структура таблицы `user_to_file`
--

CREATE TABLE `user_to_file` (
                                `id` int(11) NOT NULL,
                                `user_id` int(11) DEFAULT NULL,
                                `file_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user_to_file`
--

INSERT INTO `user_to_file` (`id`, `user_id`, `file_id`) VALUES
(2, 19, 1681304622),
(3, 18, 1681304622),
(4, 19, 1681407174);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id` (`id`);

--
-- Индексы таблицы `user_to_file`
--
ALTER TABLE `user_to_file`
    ADD PRIMARY KEY (`id`),
    ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
    MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT для таблицы `user_to_file`
--
ALTER TABLE `user_to_file`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `user_to_file`
--
ALTER TABLE `user_to_file`
    ADD CONSTRAINT `user_to_file_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
