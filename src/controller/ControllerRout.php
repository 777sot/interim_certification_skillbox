<?php


/**
 * Class ControllerRout
 */
class ControllerRout
{
    /**
     * @return mixed
     */
    function parsUrlGetEmail()
    {

        $uri = parse_url(trim($_SERVER['REQUEST_URI']), PHP_URL_PATH);

        $segments = explode('/', trim($uri, '/'));

        foreach ($segments as $key => $value) {
            if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
                return $value;
            }
        }
    }


    /**
     * @return mixed
     */
    function parsUrlGetId()
    {
        $uri = parse_url(trim($_SERVER['REQUEST_URI']), PHP_URL_PATH);

        $segments = explode('/', trim($uri, '/'));

        if (count($segments) === 4) {
            foreach ($segments as $key => $value) {
                if (is_numeric($value)) {
                    $array_id[] = $value;
                }
            }
            return $array_id;
        }

        foreach ($segments as $key => $value) {
            if (is_numeric($value)) {
                return $value;
            }
        }
    }

    /**
     * @param $urlList
     * @return mixed
     */
    public function getObjectMySQL($urlList)
    {
        if ($_REQUEST['rout']) {
            $result = [];
            foreach ($urlList as $key => $value) {

                if ($key === $_REQUEST['rout']) {
                    $result[] = $value[$_SERVER['REQUEST_METHOD']];
                    break;
                }
            }
        }
        return $result;
    }
}

//$requestMethod = $_SERVER['REQUEST_METHOD'];
//$requestUrl = $_SERVER['REQUEST_URI'];
//
//foreach ($urlList as $url => $methods) {
//    if ($requestUrl === $url && array_key_exists($requestMethod, $methods)) {
//        $controllerMethod = $methods[$requestMethod];
//        $controllerAndMethod = explode('::', $controllerMethod);
//        $controllerName = $controllerAndMethod[0];
//        $methodName = $controllerAndMethod[1];
//        $controller = new $controllerName();
//        $controller->$methodName();
//    }
//}

